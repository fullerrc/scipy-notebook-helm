## SciPy + example helm chart

Use this package to build and deploy a running instance of Jupyter Notebook,
with pre-installed SciPy libraries.  Also included is an example of using
machine learning to predict housing prices, along with the data and
dependencies to utilize it.

### Usage
1. Install Docker + Kubernetes plugin
2. Install ```kubectl```
3. Install helm
4. ```helm init```
5. ```kubectl create namespace pyspark```
6. ```docker run -d -p 5000:5000 --restart=always --name registry registry:2```
7. Set up your Docker daemon to use localhost:5000 as an insecure repository (settings -> daemon)
8. ```cd pyspark-notebook && docker build -t scipy-notebook-with-egs:0.0.2 .```
9. ```docker tag scipy-notebook-with-egs:0.0.2 localhost:5000/scipy-notebook-with-egs:0.0.2```
10. ```docker push localhost:5000/scipy-notebook-with-egs:0.0.2```
6. ```cd ../helm && helm install --namespace=pyspark --name pyspark -f ./values.yaml .```

After a few minutes, the deployment should be live, in your local Kubernetes cluster.
At that point, you'll be able to access it by browsing to http://localhost:8081 .

The token you'll need to log in can be found by entering the following command in Terminal / a shell:

```kubectl logs `kubectl get pods --namespace=pyspark | awk '{print $1;}' | grep pyspark | tail -1` --namespace=pyspark | grep token```

You can copy the value you see after ```token=``` in the output of the above command, and
paste it where asked at the login page.